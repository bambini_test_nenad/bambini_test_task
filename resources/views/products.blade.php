@extends('index')
@section('main-content')

<div class="container-fluid">	
 @if(session('success'))                   
		<div class="alert alert-success alert-dismissible fade show" role="alert">
		  <strong>{{ session('success') }}
		  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
		    <span aria-hidden="true">&times;</span>
		  </button>
		</div>
 @endif
	<div class="row justify-content-center mt-4">
		 <div class="col-lg-12">
			<div class="card">
				  <div class="card-body">
				    <h5 class="card-title">Products</h5>
						<table class="table table-striped">
						  <thead>
						    <tr>
						      <th scope="col">Name</th>
						      <th scope="col">Price</th>
						      <th scope="col">Brand</th>
						      <th scope="col"></th>
						    </tr>
						  </thead>
						  <tbody>
						  	@foreach($products as $product)
							   <tr>
							      <th scope="row">{{$product->name}}</th>
							      <td>{{$product->price}}</td>
							      <td>{{$product->brand->name}}</td>
							      <td><a href="{{route('checkout', $product->id)}}" class="btn btn-primary">Buy</a></td>
							   </tr>
						    @endforeach
						  </tbody>
						</table>
					 </div>				 
			 </div>
		</div>
	</div>
	<div class="row justify-content-center mt-4">
		<div class="col-lg-12">
		 	{{ $products->links() }}
		</div>
	</div> 	
</div>	
@endsection