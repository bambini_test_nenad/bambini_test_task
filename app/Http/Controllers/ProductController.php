<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Order;
use Mail;
use App\Mail\OrderInfoMail;
class ProductController extends Controller
{
    
  public function products(){   
  	 $products=Product::with('brand')->orderBy('created_at', 'desc')->paginate(4);	
	   return view('products')->with('products', $products);
  }





  public function checkout(Request $request,$id){ 
   
   $product=Product::with('brand')->where('id', '=', $id)->first();

   // Enter Your Stripe Secret
    \Stripe\Stripe::setApiKey('sk_test_51HgUKbH9BztYyZs80h9VeKjbX4EmZxahBV80PnGE1acLvvh3d8FbJBUwbfhNri7sm7yC2ZVlvl9wmWcM4q2OcdOw00JoC1ZSPK');

      $amount = $product->price;
      $amount *= 100;
      $amount = (int) $amount;
     
      $payment_intent = \Stripe\PaymentIntent::create([
        'description' => 'Stripe Test Payment',
        'amount' => $amount,
        'currency' => 'EUR',
        'description' => 'Payment From Codehunger',
        'payment_method_types' => ['card'],
      ]);
      $intent = $payment_intent->client_secret;
      return view('checkout')->with('intent', $intent)->with('product', $product);
  }

  public function checkoutStore(Request $request){  

    $order= new Order;
    $order->client_name=$request->name;
    $order->client_address=$request->address;
    if($request->shipping_option == 2){
      $order->total_shipping_value=10;
    }else{
      $order->total_shipping_value=0;  
    } 
    $order->total_product_value=$request->price;
    $order->save();
    

    $data = array(
            'name'      =>  $order->client_name,
            'client_address'   =>   $order->client_address,
            'total_shipping_value'   =>   $order->total_shipping_value,
            'total_product_value'   =>   $order->total_product_value,
    );

    Mail::to('bambinitestemail@gmail.com')->send(new OrderInfoMail($data));

     return redirect()->route('products')->with("success", "Your purchase process is complete");
  }
}
