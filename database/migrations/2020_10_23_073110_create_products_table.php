<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('price')->nullable();
            $table->bigInteger('brand_id')->unsigned();
            $table->timestamps();
            $table->foreign('brand_id', 'brands_ibfk_1')->references('id')->on('brands')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {   

        Schema::table('products', function (Blueprint $table) {
            $table->dropForeign('brands_ibfk_1');
            $table->dropIndex('brands_ibfk_1');
        });
        Schema::dropIfExists('products');

    }
}
