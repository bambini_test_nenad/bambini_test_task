<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;
class AddProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	     Product::updateOrCreate(
	        ['id' => 1],
	        ['name' => 'Air Max 90 shoes',
	         'price' => '120', 
	         'brand_id' => 1
	    	]
	     );
	     Product::updateOrCreate(
	        ['id' => 2],
	        ['name' => 'Classic shoes',
	         'price' => '80', 
	         'brand_id' => 4
	    	]
	     );
	     Product::updateOrCreate(
	        ['id' => 3],
	        ['name' => '574 shoes',
	         'price' => '100', 
	         'brand_id' => 3
	    	]
	     );
	    
	    Product::updateOrCreate(
	        ['id' => 4],
	        ['name' => 'ZTE shoes',
	         'price' => '90', 
	         'brand_id' => 2
	    	]
	     );
	    

	     Product::updateOrCreate(
	        ['id' => 5],
	        ['name' => 'Blue T-shirt',
	         'price' => '20', 
	         'brand_id' => 1
	    	]
	     );
	     Product::updateOrCreate(
	        ['id' => 6],
	        ['name' => 'Green T-shirt',
	         'price' => '30', 
	         'brand_id' => 2
	    	]
	     );
	     Product::updateOrCreate(
	        ['id' => 7],
	        ['name' => 'Red Sweatshirt',
	         'price' => '40', 
	         'brand_id' => 3
	    	]
	     );
	   
	    Product::updateOrCreate(
	        ['id' => 8],
	        ['name' => 'Blue Sweatshirt',
	         'price' => '50', 
	         'brand_id' => 4
	    	]
	     );
    }
}
