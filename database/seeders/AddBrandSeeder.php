<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Brand;
class AddBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Brand::updateOrCreate(
        ['id' => 1],
        ['name' => 'Nike']
      );
      Brand::updateOrCreate(
        ['id' => 2],
        ['name' => 'Adidas']
      );
      Brand::updateOrCreate(
        ['id' => 3],
        ['name' => 'New Balance']
      );
       Brand::updateOrCreate(
        ['id' => 4],
        ['name' => 'Reebok']
      );
    }
}
