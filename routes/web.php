<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get('', ['as' => 'products', 'uses' => 'App\Http\Controllers\ProductController@products']);
Route::get('/checkout/{id}', ['as' => 'checkout', 'uses' => 'App\Http\Controllers\ProductController@checkout']);
Route::post('/checkout/store', ['as' => 'checkout.store', 'uses' => 'App\Http\Controllers\ProductController@checkoutStore']);